# Flectra Community / l10n-netherlands

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[l10n_nl_postcode](l10n_nl_postcode/) | 2.0.1.0.0| Dutch postcode validation for Partners
[l10n_nl_tax_statement](l10n_nl_tax_statement/) | 2.0.1.0.0| Netherlands BTW Statement
[l10n_nl_xaf_auditfile_export](l10n_nl_xaf_auditfile_export/) | 2.0.1.3.0| Export XAF auditfiles for Dutch tax authorities
[l10n_nl_tax_statement_icp](l10n_nl_tax_statement_icp/) | 2.0.1.0.0| Netherlands ICP Statement
[l10n_nl_bsn](l10n_nl_bsn/) | 2.0.1.0.0| Burgerservicenummer (BSN) for Partners
[l10n_nl_account_tax_unece](l10n_nl_account_tax_unece/) | 2.0.1.0.0| Auto-configure UNECE params on Dutch taxes
[l10n_nl_oin](l10n_nl_oin/) | 2.0.1.0.0| Adds Dutch OIN field
[l10n_nl_bank](l10n_nl_bank/) | 2.0.1.0.0| Import all Dutch banks with BIC code


