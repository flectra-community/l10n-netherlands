# Copyright 2020 Onestein (<https://www.onestein.eu>)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Organisatie-identificatienummer (OIN)",
    "version": "2.0.1.0.0",
    "category": "Dutch Localization",
    "license": "AGPL-3",
    "summary": "Adds Dutch OIN field",
    "author": "Onestein, Odoo Community Association (OCA)",
    "maintainers": ["astirpe"],
    "website": "https://gitlab.com/flectra-community/l10n-netherlands",
    "data": ["views/res_partner.xml"],
    "installable": True,
}
