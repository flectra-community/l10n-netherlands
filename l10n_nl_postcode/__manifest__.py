# Copyright 2016-2020 Onestein (<http://www.onestein.eu>)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Dutch postcode validation for Partners",
    "version": "2.0.1.0.0",
    "category": "Localization",
    "author": "Onestein,Odoo Community Association (OCA)",
    "maintainers": ["astirpe"],
    "website": "https://gitlab.com/flectra-community/l10n-netherlands",
    "license": "AGPL-3",
    "depends": ["base"],
    "external_dependencies": {"python": ["python-stdnum"]},
}
